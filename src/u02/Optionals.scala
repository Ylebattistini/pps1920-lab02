package u02

object Optionals extends App {

  sealed trait Option[A] // An Optional data type
  object Option {
    case class None[A]() extends Option[A]
    case class Some[A](a: A) extends Option[A]

    def isEmpty[A](opt: Option[A]): Boolean = opt match {
      case None() => true
      case _ => false
    }

    def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
      case Some(a) => a
      case _ => orElse
    }

    def flatMap[A,B](opt: Option[A])(f:A => Option[B]): Option[B] = opt match {
      case Some(a) => f(a)
      case _ => None()
    }


    //es 8
   def filter[A,B](opt:Option[A])(pred: A=>Boolean): Option[A]=opt match{
      case Some(d) if pred(d)=> Some(d)
      case _=> None()
    }

    def map[A](opt:Option[A])(mapper: A=>Boolean): Option[Boolean] =opt match{
      case Some(d)=> Some(mapper(d))
      case _=>None()
    }

    def map2[A,B,C](opt:Option[A])(opt1: Option[B])(p: (A,B)=>C): Option[C] =(opt,opt1) match{
      case (Some(d),Some(d1))=> Some(p(d,d1))
      case (_, None())=>None()
      case (None(), _)=> None()
    }
  }

  import Option._
  val s1: Option[Int] = Some(1)
  val s2: Option[Int] = Some(2)
  val s3: Option[Int] = None()

  println(s1) // Some(1)
  println(getOrElse(s1,0), getOrElse(s3,0)) // 1,0
  println(flatMap(s1)(i => Some(i+1))) // Some(2)
  println(flatMap(s1)(i => flatMap(s2)(j => Some(i+j)))) // Some(3)
  println(flatMap(s1)(i => flatMap(s3)(j => Some(i+j)))) // None

  println("ES 8 -> FILTER")
  println(filter(Some(5))(_>2))//Some(5)
  println(filter(None[Int])(_>8))//None()

  println("ES 8 -> MAP")
  println(map(Some(5))(_>2))//Some(true)
  println(map(None[Int])(_>2))//None()

}
