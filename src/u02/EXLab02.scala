package u02

object EXLab02 extends App {

  //es part 3a
  //method syntax
  def parity_method(x: Int): String = x % 2 match {
    case 0 => "even"
    case _ => "odd"
  }

  //val assigned
  val parity: Int => String = {
    case x if x % 2 == 0 => "even"
    case _ => "odd"
  }
  println("ES 3a -> Parity")
  println(parity(4))//even
  println(parity(3))//odd
  println(parity_method(6))//even
  println(parity_method(5))//odd


  //es part 3b
  //val lambda
  val neg: (String => Boolean) => (String => Boolean) = (p) => (s => (!p(s)))

  //method syntax
  def negative(e: String => Boolean): (String => Boolean) = (s: String) => !e(s)

  val empty: String => Boolean = _ == ""
  val notEmpty = neg(empty)
  println("ES 3b -> Negative")
  println(notEmpty("foo"))// true
  println(notEmpty("") )// false
  println(notEmpty("foo") && !notEmpty("")) //true


  //es part 3c
  def negativeGeneral[A](e:A=>Boolean):(A=>Boolean) = (s:A) => !e(s)

  //4 currying
  //p1
  val curringLessEquales: Int =>Int =>Int => Boolean = x => y=>z=> (x<=y && y<=z)
  //p2
  val notcurringLessEquales: (Int, Int, Int) => Boolean = (x, y, z) => (x<=y && y<=z)
  //p3
  def curringLessEqual(x:Int)(y: Int)(z: Int): Boolean = (x<=y && y<=z)
  //p4
  def notcurringLessEqual(x: Int, y:Int, z:Int): Boolean = (x<=y && y<=z)

  println("ES 4 -> Currying")
  println(curringLessEquales(5)(7)(11))//true
  println(curringLessEquales(11)(7)(5))//false
  println(notcurringLessEquales(5,7,11))//true
  println(notcurringLessEquales(11,7,5))//false
  println(curringLessEqual(5)(7)(11))//true
  println(curringLessEqual(11)(7)(5))//false
  println(notcurringLessEqual(5,7,11))//true
  println(notcurringLessEqual(11,7,5))//false


  //5 functional composition
  val compose: (Int => Int, Int => Int, Int) => Int = (f1, f2, n) => f1(f2(n)) // compose(f: Int => Int, g: Int => Int): Int => Int
  println("ES 5 -> Compose")
  println(compose(_-1,_*2,5))//9

  //6 recursion
  def fibonacci(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fibonacci(n-1) + fibonacci(n - 2) //non è una ricorsione tail perchè dopo che eseguo fibonacci di n-1 devo eseguire la somma
  }
  println("ES 6 -> Fibonacci")
  println(fibonacci(0), fibonacci(1), fibonacci(2), fibonacci(3), fibonacci(4))

  //7 part4
  sealed trait Shape

  object Shape {

    case class Rectangle(b: Int, h: Int) extends Shape

    case class Circle(r: Double) extends Shape

    case class Square(l: Int) extends Shape


    def perimeter(shape: Shape): Int = shape match {
      case Rectangle(b, h) => (b + h) * 2
      case Square(l) => l * 4
      case _ => 0
    }

    def area(shape: Shape): Int = shape match {
      case Rectangle(b, h) => b * h
      case Square(l) => l * l
      case _ => 0
    }

    def circleCirconf(shape: Shape): Double = shape match {
      case Circle(r) => 2 * r * (3.14)
      case _ => 0.0
    }

    def circleArea(shape: Shape): Double = shape match {
      case Circle(r) => r * r * (3.14)
      case _ => 0.0
    }
  }

}
