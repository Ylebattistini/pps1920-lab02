package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.EXLab02.{empty, neg}


class testingClass {

  import EXLab02.Shape._
  val square= Square(5)
  val rectangle= Rectangle(6, 3)
  val circle= Circle(3.1)


  @Test def testPerimeter(){
    assertEquals(20, perimeter(square))
    assertEquals(18, perimeter(rectangle))
  }

  @Test def testArea(){
    assertEquals(25, area(square))
    assertEquals(18, area(rectangle))
  }

  @Test def testCirconf(){
    assertEquals(19.468, circleCirconf(circle))// opp 28.26
  }

  @Test def testCircleArea(){
    assertEquals(30.175400000000003, circleArea(circle))// opp 28.26
  }

}
